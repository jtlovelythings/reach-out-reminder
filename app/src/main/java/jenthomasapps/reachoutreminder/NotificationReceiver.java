package jenthomasapps.reachoutreminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class NotificationReceiver extends BroadcastReceiver {
    //Logcat tag
    public String LOGCAT_TAG = "FromCodeForDebugging";
    public NotificationReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.i(LOGCAT_TAG, "Called from the NotificationReceiver onReceive method");
    }
}
