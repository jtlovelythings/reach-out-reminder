package jenthomasapps.reachoutreminder;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import static android.content.Context.NOTIFICATION_SERVICE;
import static android.content.Intent.ACTION_DIAL;
import static android.content.Intent.ACTION_SENDTO;

public class AlarmReceiver extends BroadcastReceiver {

    //Logcat tag
    public String LOGCAT_TAG = "FromCodeForDebugging";


    private static final int NOTIFICATION_ID = 0;
    private static final String PRIMARY_CHANNEL_ID = "primary_notification_channel";
    private NotificationManager mNotificationManager;



    @Override
    public void onReceive(Context context, Intent intent) {
        mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        Log.i(LOGCAT_TAG, "Called from inside AlarmReceiver's onReceive method.");


        deliverNotification(context, intent);
    }

    //TODO: Pass data from Main to this notification. Currently data is null.
    private void deliverNotification(Context context, Intent data) {
        Log.i(LOGCAT_TAG, "Called from inside AlarmReceiver's deliverNotification method.");

        String phoneNumber = String.format("tel: %s", data.getStringExtra("pN"));
        String textNumber = String.format("smsto: %s", data.getStringExtra("pN"));
        String email = String.format("mailto: %s", data.getStringExtra("eM"));
        String firstName = data.getStringExtra("fN");
        String lastName = data.getStringExtra("lN");

        Log.i(LOGCAT_TAG, "phoneNumber: " + phoneNumber);
        Log.i(LOGCAT_TAG, "textNumber: " + textNumber);
        Log.i(LOGCAT_TAG, "email: " + email);
        Log.i(LOGCAT_TAG, "first name: " + firstName);
        Log.i(LOGCAT_TAG, "last name: " + lastName);

        Intent callIntent = new Intent(ACTION_DIAL);
        callIntent.setData(Uri.parse(phoneNumber));
        PendingIntent callPendingIntent = PendingIntent.getActivity(context, NOTIFICATION_ID, callIntent, PendingIntent.FLAG_ONE_SHOT);

        Intent textIntent = new Intent(ACTION_SENDTO);
        textIntent.setData(Uri.parse(textNumber));
        PendingIntent textPendingIntent = PendingIntent.getActivity(context, NOTIFICATION_ID, textIntent, PendingIntent.FLAG_ONE_SHOT);

        Intent emailIntent = new Intent(ACTION_SENDTO);
        emailIntent.setData(Uri.parse(email));
        PendingIntent emailPendingIntent = PendingIntent.getActivity(context, NOTIFICATION_ID, emailIntent, PendingIntent.FLAG_ONE_SHOT);

        //this starts MainActivity when the user clicks the notification
        Intent openAppIntent = new Intent(context, MainActivity.class);

        PendingIntent openAppPendingIntent = PendingIntent.getActivity
                (context, NOTIFICATION_ID, openAppIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        //notification builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, PRIMARY_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_reach_out)
                .setContentTitle("Reach Out Now")
                .setContentText("It\'s time to reach out to" + " " + firstName + ".")
                .setContentIntent(openAppPendingIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setDefaults(NotificationCompat.DEFAULT_ALL);

        builder.addAction(R.drawable.ic_call, "Call" + " " + firstName, callPendingIntent);
        builder.addAction(R.drawable.ic_text, "Text" + " " + firstName, textPendingIntent);
        builder.addAction(R.drawable.ic_email, "Email" + " " + firstName, emailPendingIntent);
        mNotificationManager.notify(NOTIFICATION_ID, builder.build());

    }//end deliverNotification

}//end AlarmReceiver
