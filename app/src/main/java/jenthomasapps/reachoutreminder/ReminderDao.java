package jenthomasapps.reachoutreminder;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ReminderDao {

    //insert a reminder
    @Insert
    void insertReminder(Reminder reminder);


    //update (or edit) a reminder
    @Update
    void updateReminder(Reminder reminder);


    //delete a reminder
    @Delete
    void deleteReminder(Reminder reminder);


    //insert multiple reminders (turn multiple reminder times and tyoes for the same person into a list
    @Insert
    void insertMultipleReminders(Reminder... reminders);

    @Query("SELECT * FROM reminder")
    LiveData<List<Reminder>> listFullNames();

    @Query("DELETE FROM reminder")
    void deleteAll();

    // @Query("SELECT reminderDate FROM reminder")
    //LiveData<List<Reminder>> listDates();


}
