package jenthomasapps.reachoutreminder;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.Date;

@Database(entities = {Reminder.class}, version = 2)
@TypeConverters({Converters.class})
public abstract class ReminderDatabase extends RoomDatabase {
    private static ReminderDatabase INSTANCE;
    private static ReminderDatabase.Callback rReminderDatabaseCallback = new ReminderDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            new PopulateDbAsync(INSTANCE).execute();
        }
    };

    public static ReminderDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ReminderDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ReminderDatabase.class, "reminder_database")
                            .addCallback(rReminderDatabaseCallback)

                            .build();
                }
            }
        }
        return INSTANCE;
    }//end get database method

    public abstract ReminderDao reminderDao();

    /**
     * Populate the database in the background.
     */
    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final ReminderDao rDao;

        PopulateDbAsync(ReminderDatabase db) {
            rDao = db.reminderDao();
        }


        @Override
        protected Void doInBackground(final Void... params) {
            // Start the app with a clean database every time.
            // Not needed if you only populate on creation.

//this deletes all reminders
            rDao.deleteAll();

            //add some reminders for testing
            //this works!  It adds the date to the Database.
            Date today = new Date();
            today.getTime();

            Reminder reminder = new Reminder("Jenny", "206", "jennythomas71@gmail.com", "2064650395", "2064650395", "Birthday", today, "monthly");
            rDao.insertReminder(reminder);
            reminder = new Reminder("Virginia", "", "vlarkey7@gmail.com", "2069410798", "2069410798", "Birthday", today, "monthly");
            rDao.insertReminder(reminder);


            return null;
        }
    }

}
