package jenthomasapps.reachoutreminder;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class ReminderRepository {

    //variable that holds reference to dao
    private ReminderDao rDao;

    //variable that holds the list of full names from the dao
    private LiveData<List<Reminder>> fullNames;


    //constructor that gets a handle to the database and initializes the member variables
    ReminderRepository(Application application) {
        ReminderDatabase rdb = ReminderDatabase.getDatabase(application);
        rDao = rdb.reminderDao();
        fullNames = rDao.listFullNames();

    }

    //wrapper for listFullNames and insert methods
    LiveData<List<Reminder>> listFullNames() {
        return fullNames;
    }

    //wrapper for listDates


    //wrappers for insert methods async tasks
    public void insertReminder(Reminder reminder) {
        new insertAsyncTask(rDao).execute(reminder);
    }

    public void insertMultipleReminders(Reminder... reminders) {
        new insertAsyncTask(rDao).execute(reminders);
    }

    //wrapper for delete reminder method
    public void deleteReminder(Reminder reminder) {
        new deleteAsyncTask(rDao).execute(reminder);

    }


    //wrapper for update method
    public void updateReminder(Reminder reminder) {
        new updateAsyncTask(rDao).execute(reminder);
    }

    //async insert task class
    private static class insertAsyncTask extends AsyncTask<Reminder, Void, Void> {

        private ReminderDao rAsyncTaskDao;

        insertAsyncTask(ReminderDao dao) {
            rAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Reminder... params) {
            rAsyncTaskDao.insertReminder(params[0]);

            return null;
        }


    }//end async class

    private static class deleteAsyncTask extends android.os.AsyncTask<Reminder, Void, Void> {

        private ReminderDao dAsyncTaskDao;

        deleteAsyncTask(ReminderDao dao) {
            dAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Reminder... params) {
            dAsyncTaskDao.deleteReminder(params[0]);

            return null;
        }
    }

    //async update task class
    private static class updateAsyncTask extends AsyncTask<Reminder, Void, Void> {

        private ReminderDao uAsyncTaskDao;

        updateAsyncTask(ReminderDao dao) {
            uAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Reminder... params) {
            uAsyncTaskDao.updateReminder(params[0]);

            return null;
        }
    }


}
