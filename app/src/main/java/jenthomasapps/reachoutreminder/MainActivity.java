package jenthomasapps.reachoutreminder;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static jenthomasapps.reachoutreminder.NewReminderActivity.RECURRENCE;

public class MainActivity extends AppCompatActivity {

    //Logcat tag
    public String LOGCAT_TAG = "FromCodeForDebugging";


    public static final int NEW_REMINDER_ACTIVITY_REQUEST_CODE = 1;
    public static final int EDIT_REMINDER_ACTIVITY_REQUEST_CODE = 2;


    private static final int NOTIFICATION_ID = 0;
    private static final String PRIMARY_CHANNEL_ID = "primary_notification_channel";


    private NotificationManager mNotificationManager;
    //reference to NotificationReceiver class
    //private NotificationReceiver reminderNotificationReceiver = new NotificationReceiver();
    //private static final String ACTION_ALARM_NOTIFICATION = "jenthomasapps.reachoutreminder.ACTION_ALARM_NOTIFICATION";
    //reference to reminder view model
    private ReminderViewModel rReminderVM;


    public static final String ID_STRING = "id";
    public static final String FULL_NAME = "fullName";
    public static final String WORK_PHONE = "wPhone";
    public static final String EMAIL = "email";
    public static final String MOBILE_PHONE = "mPhone";
    public static final String HOME_PHONE = "hPhone";
    public static final String TYPE = "type";
    public static String DATE_STRING = "date";
    public static String TIME_STRING = "time";
    public static final String RECURR = "recur";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        RecyclerView recyclerView = findViewById(R.id.recyclerview);

        //mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);


        //registering the broadcast receiver
        //registerReceiver(reminderNotificationReceiver, new IntentFilter(ACTION_DIAL));

        RecyclerViewDeleteClickListener listener = new RecyclerViewDeleteClickListener() {

            //deletes the reminder on delete click by calling deleteReminder and passing in the reminder
            @Override
            public void onDeleteClick(View view, int position, Reminder toDelete) {

                Log.i(LOGCAT_TAG, "Position of deleted reminder is: " + position);
                rReminderVM.deleteReminder(toDelete);
            }


        };

        RecyclerViewEditClickListener editClickListener = new RecyclerViewEditClickListener() {

            //passes the reminder to be edited data as an Intent to the EditReminderActivity and starts the activity for a result
            @Override
            public void onEditClick(View view, int position, Reminder toEdit) {

                Log.i(LOGCAT_TAG, "Position of edited reminder is: " + position);

                Intent editIntent = new Intent(MainActivity.this, EditReminderActivity.class);


                //bundling the teEdit reminder
                Bundle bundle = new Bundle();

                bundle.putString(ID_STRING, String.valueOf(toEdit.getId()));
                bundle.putString(FULL_NAME, toEdit.getFullName());
                bundle.putString(WORK_PHONE, toEdit.getWorkPhone());
                bundle.putString(EMAIL, toEdit.getEmail());
                bundle.putString(MOBILE_PHONE, toEdit.getMobilePhone());
                bundle.putString(HOME_PHONE, toEdit.getHomePhone());
                bundle.putString(TYPE, toEdit.getReminderType());
                bundle.putSerializable(DATE_STRING, toEdit.getReminderDate().toString());
                bundle.putString(RECURR, toEdit.getRepeatInterval());
                editIntent.putExtras(bundle);


                startActivityForResult(editIntent, EDIT_REMINDER_ACTIVITY_REQUEST_CODE);

            }


        };

        final ReminderListAdapter adapter = new ReminderListAdapter(this, listener, editClickListener);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewReminderActivity.class);

                startActivityForResult(intent, NEW_REMINDER_ACTIVITY_REQUEST_CODE);
            }
        });


        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        rReminderVM = ViewModelProviders.of(this).get(ReminderViewModel.class);

        rReminderVM.getFullNames().observe(this, new Observer<List<Reminder>>() {


            @Override
            public void onChanged(@Nullable final List<Reminder> reminders) {
                // Update the cached copy of the words in the adapter.
                adapter.setReminders(reminders);
            }
        });
        //creates the notification channel
        createNotificationChannel();
    }//end onCreate


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //takes in activity result data based on the request codes of the if statement - either adds a new reminder to the database or replaces the edited data
    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//TODO Fix for new schema
        String mobileNumber = data.getStringExtra(NewReminderActivity.MOBILE_PHONE);
        String firstName = data.getStringExtra(NewReminderActivity.FULL_NAME);
        String workPhone = data.getStringExtra(NewReminderActivity.WORK_PHONE);
        String email = data.getStringExtra(NewReminderActivity.EMAIL);
        String type = data.getStringExtra(NewReminderActivity.TYPE);
        String recurrence = data.getStringExtra(NewReminderActivity.RECURRENCE);



        Intent alarmNotificationIntent = new Intent(this, AlarmReceiver.class);


        alarmNotificationIntent.putExtra("mN", mobileNumber);
        alarmNotificationIntent.putExtra("fN", firstName);
        alarmNotificationIntent.putExtra("lN", workPhone);
        alarmNotificationIntent.putExtra("eM", email);
        alarmNotificationIntent.putExtra("tY", type);
        alarmNotificationIntent.putExtra("rec", recurrence);


        //alarmNotificationIntent.setData(Uri.parse(phoneNumber));
        PendingIntent alarmNotificationPendingIntent = PendingIntent.getBroadcast(this, NOTIFICATION_ID, alarmNotificationIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);


        if (requestCode == NEW_REMINDER_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {


            Reminder reminder = new Reminder(data.getStringExtra(NewReminderActivity.FULL_NAME),
                    data.getStringExtra(NewReminderActivity.WORK_PHONE), data.getStringExtra(NewReminderActivity.EMAIL),
                    data.getStringExtra(NewReminderActivity.MOBILE_PHONE), data.getStringExtra(NewReminderActivity.TYPE), data.getStringExtra(NewReminderActivity.HOME_PHONE), null, data.getStringExtra(RECURRENCE));

            Date date = (Date) data.getSerializableExtra(NewReminderActivity.DATE_STRING);
            if (date != null) {

                Log.i(LOGCAT_TAG, "Date is: " + date.toString());
            }
            reminder.setReminderDate(date);


            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);


            rReminderVM.insertReminder(reminder);


            //set an alarm based on the date time string of the reminder that was added

            long triggerTime = SystemClock.elapsedRealtime();


            long repeatInterval = 60 * 1000;
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, triggerTime, repeatInterval, alarmNotificationPendingIntent);

            //right now this delivers the notification when a new reminder is inserted
            //deliverNotification(this, data);

        }//end onActivityResult for insertReminder

        if (requestCode == EDIT_REMINDER_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {


            Date date = (Date) data.getSerializableExtra(EditReminderActivity.EDITED_DATE_STRING);


            //Get the edited reminder data and replace it based on the reminder id
            //follow above
            Reminder editedReminder = new Reminder(data.getStringExtra(EditReminderActivity.EDITED_FULL_NAME),
                    data.getStringExtra(EditReminderActivity.EDITED_WORK_PHONE), data.getStringExtra(EditReminderActivity.EDITED_EMAIL),
                    data.getStringExtra(EditReminderActivity.EDITED_MOBILE_PHONE), data.getStringExtra(EditReminderActivity.EDITED_TYPE), data.getStringExtra(EditReminderActivity.EDITED_HOME_PHONE), date, data.getStringExtra(EditReminderActivity.EDITED_RECURRENCE));

            editedReminder.setReminderDate(date);
            if (date != null) {

                Log.i(LOGCAT_TAG, "Date of edited reminder: " + date.toString());
            }

            int editedReminderId = Integer.parseInt(data.getStringExtra(EditReminderActivity.REMINDER_ID));

            editedReminder.setId(editedReminderId);
            rReminderVM.updateReminder(editedReminder);


        }
    }//end onActivityResult

    public void createNotificationChannel() {

        // Create a notification manager object.
        mNotificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Notification channels are only available in OREO and higher.
        // So, add a check on SDK version.
        if (android.os.Build.VERSION.SDK_INT >=
                android.os.Build.VERSION_CODES.O) {

            // Create the NotificationChannel with all the parameters.
            NotificationChannel notificationChannel = new NotificationChannel
                    (PRIMARY_CHANNEL_ID,
                            "Reach Out Reminder Notification",
                            NotificationManager.IMPORTANCE_HIGH);

            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription
                    ("Notifies user to call, text, or email");
            mNotificationManager.createNotificationChannel(notificationChannel);
        }


    }//end createNotificationChannel





}//end Main Activity
