package jenthomasapps.reachoutreminder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ReminderListAdapter extends RecyclerView.Adapter<ReminderListAdapter.ReminderViewHolder> {

    //class variables
    private final LayoutInflater mInflater;
    private RecyclerViewDeleteClickListener rListener;
    private RecyclerViewEditClickListener eListener;

    private List<Reminder> rReminderList = new ArrayList<>();


    ReminderListAdapter(Context context, RecyclerViewDeleteClickListener deleteClickListener, RecyclerViewEditClickListener editClickListener) {
        mInflater = LayoutInflater.from(context);
        rListener = deleteClickListener;
        eListener = editClickListener;
    }


    @Override
    public ReminderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new ReminderViewHolder(itemView, rListener, eListener);


    }


    @Override
    public void onBindViewHolder(final ReminderViewHolder holder, final int position) {


        if (rReminderList != null) {
            final Reminder current = rReminderList.get(position);


            holder.reminderFN.setText(current.getFullName());
            holder.reminderWP.setText(current.getWorkPhone());
            holder.reminderRepeat.setText(current.getRepeatInterval());


            if (!(holder.reminderDT.getText() == null)) {
                //holder.reminderDT.setText(current.getReminderDate().toString());
                holder.reminderDT.setText(String.valueOf(current.getReminderDate()));

            }


        } else {
            // Covers the case of data not being ready yet.
            holder.reminderFN.setText("No First Name");
            holder.reminderWP.setText("No Work Phone");
            holder.reminderDT.setText("No Date yet");

        }


    }


    void setReminders(List<Reminder> reminders) {
        rReminderList.clear();
        rReminderList.addAll(reminders);

        notifyDataSetChanged();
    }


    // getItemCount() is called many times, and when it is first called,
    // list has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (rReminderList != null)
            return rReminderList.size();
        else return 0;
    }


    //method to get a reminder at a certain position
    public Reminder getReminderAtPosition(int position) {

        return rReminderList.get(position);
    }


    class ReminderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView reminderFN;
        private final TextView reminderWP;
        private final TextView reminderDT;
        private final TextView reminderRepeat;
        private final Button deleteBtn;
        private final Button editBtn;

        //reference to recyclerviewclicklistener
        private RecyclerViewDeleteClickListener dListener;
        private RecyclerViewEditClickListener eListener;

        private ReminderViewHolder(View itemView, RecyclerViewDeleteClickListener listener, final RecyclerViewEditClickListener editListener) {
            super(itemView);
            dListener = listener;
            eListener = editListener;
            reminderFN = itemView.findViewById(R.id.textViewFirstName);
            reminderWP = itemView.findViewById(R.id.textViewLastName);
            reminderDT = itemView.findViewById(R.id.textViewDate);
            reminderRepeat = itemView.findViewById(R.id.textViewInterval);
            deleteBtn = itemView.findViewById(R.id.button_delete);
            editBtn = itemView.findViewById(R.id.button_edit);


            deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dListener.onDeleteClick(view, getAdapterPosition(), getReminderAtPosition(getAdapterPosition()));
                }
            });

            editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    eListener.onEditClick(view, getAdapterPosition(), getReminderAtPosition(getAdapterPosition()));
                }
            });

        }


        @Override
        public void onClick(View v) {

        }
    }//end inner ReminderViewHolder class


}//end class
