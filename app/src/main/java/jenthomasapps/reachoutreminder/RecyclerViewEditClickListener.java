package jenthomasapps.reachoutreminder;

import android.view.View;

public abstract class RecyclerViewEditClickListener {

    abstract void onEditClick(View view, int position, Reminder toEdit);
}
