package jenthomasapps.reachoutreminder;

import android.view.View;

public abstract class RecyclerViewDeleteClickListener {

    abstract void onDeleteClick(View view, int position, Reminder toDelete);


}

