package jenthomasapps.reachoutreminder;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

public class NewReminderActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    static final int PICK_CONTACT_REQUEST = 13;
    //Logcat tag
    public String LOGCAT_TAG = "FromCodeForDebugging";
//change for testing

    public static final String FULL_NAME = "fullName";
    public static final String WORK_PHONE = "workPhone";
    public static final String EMAIL = "email";
    public static final String MOBILE_PHONE = "mobilePhone";
    public static final String TYPE = "type";
    public static final String DATE_STRING = "date";
    public static final String TIME_STRING = "time";
    public static final String HOME_PHONE = "homePhone";
    public static final String RECURRENCE = "recurrence";
    Calendar cal = Calendar.getInstance();
    private EditText etFullName, etWorkPhone, etEmail, etMobilePhone, etType, etRecurrence, etHomePhone;
    private Date localSavedData;
    private Button btnDate;
    private Button btnTime;

    private TextView txtDate;
    private TextView txtTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_reminder);
        etFullName = findViewById(R.id.edit_fullName);
        etWorkPhone = findViewById(R.id.edit_workPhone);
        etEmail = findViewById(R.id.edit_email);
        etMobilePhone = findViewById(R.id.edit_mobilePhone);
        btnDate = findViewById(R.id.button_date);
        btnTime = findViewById(R.id.button_time);
        etHomePhone = findViewById(R.id.edit_homePhone);
        etType = findViewById(R.id.edit_type);
        etRecurrence = findViewById(R.id.edit_recurrence);
        txtDate = findViewById(R.id.textView_Date);
        txtTime = findViewById(R.id.textView_Time);

        final Button saveButton = findViewById(R.id.button_save);
        final Button btnContacts = findViewById(R.id.button_Pick_Contacts);

        //when the save saveButton is clicked, it sends a result to the Main Activity
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent saveIntent = new Intent();
                Bundle bundle = new Bundle();

                if (localSavedData == null) {

                    Toast.makeText(
                            getApplicationContext(),
                            "Needs a date",
                            Toast.LENGTH_LONG).show();


                    setResult(RESULT_CANCELED, saveIntent);
                } else {


                    bundle.putString(FULL_NAME, etFullName.getText().toString());
                    bundle.putString(WORK_PHONE, etWorkPhone.getText().toString());
                    bundle.putString(EMAIL, etEmail.getText().toString());
                    bundle.putString(MOBILE_PHONE, etMobilePhone.getText().toString());
                    bundle.putString(TYPE, etType.getText().toString());
                    bundle.putString(RECURRENCE, etRecurrence.getText().toString());
                    bundle.putString(HOME_PHONE, etHomePhone.getText().toString());
                    if (localSavedData != null) {
                        bundle.putSerializable(DATE_STRING, localSavedData);
                    }

                    if (localSavedData != null) {
                        bundle.putSerializable(TIME_STRING, localSavedData);
                    }

                    saveIntent.putExtras(bundle);
                    setResult(RESULT_OK, saveIntent);
                }
                finish();

            }
        });

        //onClickListener for contacts button
        btnContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickContact();
            }
        });


    }

    private void pickContact() {
        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);


        startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        // Check which request we're responding to
        if (requestCode == PICK_CONTACT_REQUEST) {


            // Make sure the request was successful

            if (resultCode == RESULT_OK) {


                try {


                    Log.i(LOGCAT_TAG, "result code: " + resultCode);
                    // The user picked a contact.

                    // The Intent's data Uri identifies which contact was selected.
                    Uri contactUri = data.getData();
                    Log.i(LOGCAT_TAG, "contactUri: " + contactUri);

                    String id = contactUri.getLastPathSegment();


                    getContactName(id);
                    getContactEmail(id);
                    getContactPhoneNumbers(id);


                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else {
                Log.e(LOGCAT_TAG, "Failed to pick contact.");
            }
        }
    }

    private void getContactName(String id) {
        Cursor cursor;
        String contactName = "";
        try {
            cursor = getContentResolver().query(ContactsContract
                            .Data.CONTENT_URI,
                    null,
                    ContactsContract.Data.CONTACT_ID + "=?",
                    new String[]{id},
                    null);
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(cursor.getColumnIndex
                        (ContactsContract.Data.DISPLAY_NAME));
                cursor.close();
            }
        } catch (Exception e) {

        }
        String cName = contactName;
        Log.i(LOGCAT_TAG, "Contact name: " + contactName);
        etFullName.setText(cName);
    }//end getContactName

    private void getContactEmail(String id) {
        Cursor cursor;
        String contactEmail = "";
        try {
            cursor = getContentResolver().query(ContactsContract
                            .Data.CONTENT_URI,
                    null,
                    ContactsContract.Data.CONTACT_ID + "=? AND " +
                            ContactsContract.Data.MIMETYPE + "=?",
                    new String[]{id, ContactsContract.CommonDataKinds
                            .Email.CONTENT_ITEM_TYPE},
                    null);
            if (cursor.moveToFirst()) {
                contactEmail = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                cursor.close();

            }

        } catch (Exception e) {

        }//end try/catch
        String cEmail = contactEmail;
        Log.i(LOGCAT_TAG, "Contact email: " + contactEmail);
        etEmail.setText(cEmail);

    }//end getContactEmail


    private void getContactPhoneNumbers(String id) {
        Cursor cursor;
        String contactHomePhone = "";

        String contactMobilePhone = "";

        String contactWorkPhone = "";


        try {
            cursor = getContentResolver().query(ContactsContract
                            .Data.CONTENT_URI,
                    null,
                    ContactsContract.Data.CONTACT_ID + "=? AND " +
                            ContactsContract.Data.MIMETYPE + "=?",
                    new String[]{id, ContactsContract.CommonDataKinds
                            .Phone.CONTENT_ITEM_TYPE},
                    null);
            cursor.moveToFirst();
            while (cursor.moveToNext()) {
                String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                int t = ContactsContract.CommonDataKinds.Phone.TYPE_COMPANY_MAIN;
                try {
                    String typeStr = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                    t = Integer.parseInt(typeStr);
                } catch (Exception ex) {
                }
                switch (t) {
                    case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                        Log.i(LOGCAT_TAG, "from Type work " + number);
                        break;

                    case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                        Log.i(LOGCAT_TAG, "from Type home " + number);
                        break;

                    case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                        Log.i(LOGCAT_TAG, "from Type mobile " + number);

                    default:

                        break;
                }
            }
        } catch (Exception ex) {

            ex.printStackTrace();
        }


    }//end getContactEmail


    public void showDatePickerDialog(View v) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }


    public void showTimePickerDialog(View v) {
        TimePickerFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }


    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        // Do something with the date chosen by the user
        //Calendar cal = Calendar.getInstance();


        cal.set(year, month, dayOfMonth);


        localSavedData = cal.getTime();


        txtDate.setText(localSavedData.toString());

    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        //Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minute);

        localSavedData = cal.getTime();

        txtTime.setText(localSavedData.toString());
    }


}
