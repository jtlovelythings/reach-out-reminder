package jenthomasapps.reachoutreminder;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static jenthomasapps.reachoutreminder.MainActivity.DATE_STRING;

public class EditReminderActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    public static final String EDITED_FULL_NAME = "fullName";
    private TextView editDate, editTime;
    private Button buttonEditDate, buttonEditTime, buttonSaveEdit;

    public static final String REMINDER_ID = "id";
    public static final String EDITED_WORK_PHONE = "workPhone";
    public static final String EDITED_MOBILE_PHONE = "mobilePhone";
    public static final String EDITED_EMAIL = "email";
    public static final String EDITED_HOME_PHONE = "phone";
    private EditText editFN, editWP, editMobilePhone, editEmail, editType, editRec, editHomePhone;
    public static final String EDITED_TYPE = "type";
    public static String EDITED_DATE_STRING = "date";
    public static String EDITED_TIME_STRING = "time";
    public static final String EDITED_RECURRENCE = "recurrence";
    Calendar cal = Calendar.getInstance();
    private Date editedLocalSavedData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_reminder);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editFN = findViewById(R.id.edit_fullNameToEdit);
        editWP = findViewById(R.id.edit_workPhoneToEdit);
        editEmail = findViewById(R.id.edit_emailToEdit);
        editMobilePhone = findViewById(R.id.edit_mobilePhoneToEdit);
        editHomePhone = findViewById(R.id.edit_homePhoneToEdit);
        buttonEditDate = findViewById(R.id.button_dateToEdit);
        buttonEditTime = findViewById(R.id.button_timeToEdit);
        editType = findViewById(R.id.edit_typeToEdit);
        editRec = findViewById(R.id.edit_recurrenceToEdit);
        editDate = findViewById(R.id.textView_DateToEdit);
        editTime = findViewById(R.id.textView_TimeToEdit);

        buttonSaveEdit = findViewById(R.id.button_saveToEdit);


        final Bundle receivedBundle = getIntent().getExtras();


        editFN.setText(receivedBundle.getString(MainActivity.FULL_NAME));
        editWP.setText(receivedBundle.getString(MainActivity.WORK_PHONE));
        editEmail.setText(receivedBundle.getString(MainActivity.EMAIL));
        editMobilePhone.setText(receivedBundle.getString(MainActivity.MOBILE_PHONE));
        editHomePhone.setText(receivedBundle.getString(MainActivity.HOME_PHONE));
        editType.setText(receivedBundle.getString(MainActivity.TYPE));
        editRec.setText(receivedBundle.getString(MainActivity.RECURR));
        editDate.setText(receivedBundle.getString((DATE_STRING)));
        editTime.setText(receivedBundle.getString(DATE_STRING));


        //when the save button is clicked
        buttonSaveEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editedIntent = new Intent();
                Bundle editedBundle = new Bundle();

                if (TextUtils.isEmpty(editFN.getText())) {
                    setResult(RESULT_CANCELED, editedIntent);
                } else {

                    editedBundle.putString(REMINDER_ID, receivedBundle.getString(MainActivity.ID_STRING));
                    editedBundle.putString(EDITED_FULL_NAME, editFN.getText().toString());
                    editedBundle.putString(EDITED_WORK_PHONE, editWP.getText().toString());
                    editedBundle.putString(EDITED_EMAIL, editEmail.getText().toString());
                    editedBundle.putString(EDITED_MOBILE_PHONE, editMobilePhone.getText().toString());
                    editedBundle.putString(EDITED_TYPE, editType.getText().toString());
                    editedBundle.putString(EDITED_RECURRENCE, editRec.getText().toString());
                    editedBundle.putString(EDITED_HOME_PHONE, editHomePhone.getText().toString());
                    if (editedLocalSavedData != null) {
                        editedBundle.putSerializable(EDITED_DATE_STRING, editedLocalSavedData);
                    }

                    if (editedLocalSavedData != null) {
                        editedBundle.putSerializable(EDITED_TIME_STRING, editedLocalSavedData);

                    } else {

                        final Bundle receivedBundle = getIntent().getExtras();


                        try {


                            SimpleDateFormat formatter = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy",
                                    Locale.ENGLISH);
                            final Date thisDate = formatter.parse(receivedBundle.getString((DATE_STRING)));
                            editedLocalSavedData = thisDate;

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        editedBundle.putSerializable(EDITED_DATE_STRING, editedLocalSavedData);
                        editedBundle.putSerializable(EDITED_TIME_STRING, editedLocalSavedData);
                    }

                    editedIntent.putExtras(editedBundle);
                    setResult(RESULT_OK, editedIntent);


                }
                finish();
            }


        });
    }


    public void showDatePickerDialog(View v) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }


    public void showTimePickerDialog(View v) {
        TimePickerFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        cal.set(year, month, dayOfMonth);
        editedLocalSavedData = cal.getTime();
        editDate.setText(editedLocalSavedData.toString());

    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minute);

        editedLocalSavedData = cal.getTime();

        editTime.setText(editedLocalSavedData.toString());
    }
}
