package jenthomasapps.reachoutreminder;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public class ReminderViewModel extends AndroidViewModel {

    //reference to repository
    private ReminderRepository rRepository;

    //private member variable to hold fullNames
    private LiveData<List<Reminder>> fullNames;

    //constructor gets a reference to the repository and gets the list of full names


    public ReminderViewModel(@NonNull Application application) {
        super(application);
        rRepository = new ReminderRepository(application);
        fullNames = rRepository.listFullNames();

    }

    LiveData<List<Reminder>> getFullNames() {
        return fullNames;
    }


    //wrappers for insert methods that call the repository's insert methods
    public void insertReminder(Reminder reminder) {
        rRepository.insertReminder(reminder);
    }

    public void insertMultipleReminders(Reminder... reminders) {
        rRepository.insertMultipleReminders(reminders);
    }

    public void deleteReminder(Reminder reminder) {
        rRepository.deleteReminder(reminder);
    }

    public void updateReminder(Reminder reminder) {
        rRepository.updateReminder(reminder);
    }

}
